///////////////////////////////////////////////////////////////////////////////
//  Copyright Christopher Kormanyos 2018.
//  Distributed under the Boost Software License,
//  Version 1.0. (See accompanying file LICENSE_1_0.txt
//  or copy at http://www.boost.org/LICENSE_1_0.txt)
//

#include <array>
#include <cstdint>
#include <mcal_cpu.h>

extern "C" void __my_startup       () __attribute__((section(".startup"), used, noinline));
extern "C" void __vector_unused_irq() __attribute__((signal, used, externally_visible));
extern "C" void __vector_21        () __attribute__((signal, used, externally_visible));
extern "C" void __vector_22        () __attribute__((signal, used, externally_visible));

extern "C"
void __vector_unused_irq()
{
    for(;;)
    {
        mcal::cpu::nop();
    }
}

namespace
{
    typedef struct struct_isr_type
    {
        typedef void(*function_type)();

        const std::uint8_t  jmp[2]; // JMP instruction (0x940C):
                                    // 0x0C = low byte, 0x94 = high byte.
        const function_type func;   // The interrupt service routine.
    } isr_type;
}

extern "C"
const volatile std::array<isr_type, 29U> __isr_vector __attribute__((section(".isr_vector")));

extern "C"
const volatile std::array<isr_type, 29U> __isr_vector =
{{
                                              // addr.  nr. interrupt source
  { { 0x0C, 0x94 }, __my_startup },           // 0x00,  0,  reset
  { { 0x0C, 0x94 }, __vector_unused_irq },    // 0x02,  1,  ext0
  { { 0x0C, 0x94 }, __vector_unused_irq },    // 0x04,  2,  ext1
  { { 0x0C, 0x94 }, __vector_unused_irq },    // 0x06,  3,  ext2
  { { 0x0C, 0x94 }, __vector_unused_irq },    // 0x08,  4,  ext3
  { { 0x0C, 0x94 }, __vector_unused_irq },    // 0x0A,  5,  ext4
  { { 0x0C, 0x94 }, __vector_unused_irq },    // 0x0C,  6,  ext5
  { { 0x0C, 0x94 }, __vector_unused_irq },    // 0x0E,  7,  ext6
  { { 0x0C, 0x94 }, __vector_unused_irq },    // 0x10,  8,  ext7
  { { 0x0C, 0x94 }, __vector_unused_irq },    // 0x12,  9,  pin change int request0
  { { 0x0C, 0x94 }, __vector_unused_irq },    // 0x14, 10,  pin change int request1
  { { 0x0C, 0x94 }, __vector_unused_irq },    // 0x16, 11,  USB general int request
  { { 0x0C, 0x94 }, __vector_unused_irq },    // 0x18, 12,  USB endpt int request
  { { 0x0C, 0x94 }, __vector_unused_irq },    // 0x1A, 13,  Watchdog timeout int
  { { 0x0C, 0x94 }, __vector_unused_irq },    // 0x1C, 14,  timer/cntr1 capture
  { { 0x0C, 0x94 }, __vector_unused_irq },    // 0x1E, 15,  timer/cntr1 compare A
  { { 0x0C, 0x94 }, __vector_unused_irq },    // 0x20, 16,  timer/cntr1 compare B
  { { 0x0C, 0x94 }, __vector_unused_irq },    // 0x22, 17,  timer/cntr1 compare C
  { { 0x0C, 0x94 }, __vector_unused_irq },    // 0x24, 18,  timer/cntr1 overflow
  { { 0x0C, 0x94 }, __vector_unused_irq },    // 0x26, 19,  timer/cntr0 compare A
  { { 0x0C, 0x94 }, __vector_unused_irq },    // 0x28, 20,  timer/cntr0 compare B
  { { 0x0C, 0x94 }, __vector_21 },            // 0x2A, 21,  timer/cntr0 overflow
  { { 0x0C, 0x94 }, __vector_22 },            // 0x2C, 22,  SPI xfer complete
  { { 0x0C, 0x94 }, __vector_unused_irq },    // 0x2E, 23,  USART1 Rx complete
  { { 0x0C, 0x94 }, __vector_unused_irq },    // 0x30, 24,  USART1 data reg empty
  { { 0x0C, 0x94 }, __vector_unused_irq },    // 0x32, 25,  USART1 Tx complete
  { { 0x0C, 0x94 }, __vector_unused_irq },    // 0x34, 26,  analog comparator
  { { 0x0C, 0x94 }, __vector_unused_irq },    // 0x36, 27,  eeprom ready
  { { 0x0C, 0x94 }, __vector_unused_irq },    // 0x38, 28,  spm ready
}};
