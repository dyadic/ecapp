///////////////////////////////////////////////////////////////////////////////
//  Copyright Dyadic Pty Ltd 2022

#include <stdio.h>
#include <array>
#include "pico/stdlib.h"
#include "pico/stdio_usb.h"
#include "hardware/gpio.h"
#include "hardware/spi.h"

#include "util_circular_buffer.h"


namespace {

    const std::uint8_t   CMD_TERMINATOR     = ':';
    const std::uint8_t   CMD_VAL_SEPARATOR  = ',';
    const std::size_t    cmdbuf_size        = 1024U;
    const std::size_t    cmdvalbuf_size     = 128U;
    const std::size_t    respbuf_size       = 16U;
    const std::uint32_t  cmdchar_timeout    = 1000U;
    const std::array<uint8_t, 16U>    hexchars
    {
        { '0', '1', '2', '3', '4', '5', '6', '7',
          '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' }
    };
    const char app_version[] = "ECApp USB command processor (Pico v0.9).";
    const char app_copyright[] = "Copyright, Dyadic Pty Ltd (2022).";

    const uint  GPIO_MISO = 4;  // Pico pin 6
    const uint  GPIO_SCK = 2;   // Pico pin 4 
    const uint  GPIO_MOSI = 3;  // Pico pin 5
    
    const std::array<uint, 6U>  spi_baudrates
    {
        {8000*1000, 4000*1000, 1000*1000, 250*1000, 125*1000}
    };
    const uint  SPI_125K = 4;
    const uint  SPI_250K = 3;
    const uint  SPI_1MEG = 2;
    const uint  SPI_4MEG = 1;
    const uint  SPI_8MEG = 0;

    // GPIOs
    //  0, 1, 5, .., 22, 26
    //  0b0000_0100_0111_1111_1111_1111_1110_0011
    //  0x047FFFE3
    //
    const std::uint32_t  GPIO_MASK = 0x047FFFE3;

    const std::uint8_t  PORTB_START = 16;
    const std::uint8_t  PORTC_START = 5;
    const std::uint8_t  PORTD_START = 8;
    const std::uint32_t  PORTB_MASK = 0x047F0000;
    const std::uint32_t  PORTC_MASK = 0x000000E3;
    const std::uint32_t  PORTD_MASK = 0x0000FF00;
    
    typedef util::circular_buffer<std::uint8_t, cmdbuf_size>   cmdbuf_type;
    typedef std::array<std::uint8_t, cmdvalbuf_size>           cmdvalbuf_type;
    typedef util::circular_buffer<std::uint8_t, respbuf_size>  respbuf_type;

    cmdbuf_type     cmd_buffer;
    cmdvalbuf_type  cmdval_buffer;
    respbuf_type    resp_buffer;

    typedef enum enum_cmdstate
    {
        awaiting_cmd,
        cmd_available,
        cmd_processed
    }
    cmdstate_type;

    cmdstate_type  cmd_state;
    std::size_t    cmd_count;
    uint           current_spi_baudrate = SPI_4MEG;

    void resp_msg(const std::string& msg)
    {
        std::for_each(msg.begin(), msg.end(), [] (char const& c) {
                                                  resp_buffer.push_front(c); });
    }

    void initialize_gpio()
    {
        // All relevant GPIOs are initialized as outputs and
        // set to low.
        gpio_init_mask(GPIO_MASK);
        gpio_set_dir_out_masked(GPIO_MASK);
        gpio_put_masked(GPIO_MASK, 0x0);
    }
    
    void initialize_spi()
    {
        spi_init(spi0, spi_baudrates[current_spi_baudrate]);
        gpio_set_function(GPIO_MISO, GPIO_FUNC_SPI);
        gpio_set_function(GPIO_SCK, GPIO_FUNC_SPI);
        gpio_set_function(GPIO_MOSI, GPIO_FUNC_SPI);
    }
    
    void cmdproc_init()
    {
        stdio_init_all();
        // Wait for USB to negotiate with host
        sleep_ms(1000);
        initialize_spi();
        initialize_gpio();
        
        cmd_buffer.clear();
        resp_buffer.clear();
        cmd_state = awaiting_cmd;
        cmd_count = 0;
    }

    /*! \brief Convert port and pin specification to a GPIO
     *
     * This maps an AVR port and pin specification to a GPIO on the Pico.
     * This allows client apps (which use the AVR port and pin spec.)
     * to transparently use the Pico with no changes.
     *
     * Note that valid port 'C' pins are {2, 4, 5, 6, 7}.  If some other
     * pin value is specified for port 'C' the result will be either
     * an exception or undefined.
     *
     * \return The GPIO number associated with the port/pin 
     *         combination
     */
    uint pin_to_gpio()
    {
        uint  gpio = 0;
        std::uint8_t  cmdCh = 0;
        std::uint8_t  pin = 0;
        std::uint8_t  port = cmd_buffer.out();
        
        if (port == 'G') {
            std::uint8_t  cnt = 0;            
            while ((cmdCh = cmd_buffer.out()) != CMD_TERMINATOR)
            {
                std::uint8_t x = cmdCh - '0';
                if (cnt > 0) {
                    pin = pin * 10;
                }
                cnt++;
                pin += x;
            }
        }
        else {
            pin = cmd_buffer.out() - '0';
            cmdCh = cmd_buffer.out();  // the CMD_TERMINATOR
        }
        
        
        switch (port)
        {
        case 'B':
            if (pin == 7) {
                gpio = 26;
            }
            else {
                gpio = pin + PORTB_START;
            }
            break;
        case 'D':
            gpio = pin + PORTD_START;
            break;
        case 'C':
            if (pin == 2) {
                gpio = 0;
            }
            else {
                const std::array<uint, 4U> portc_gpio
                    {
                        {1, 5, 6, 7}
                    };
                gpio = portc_gpio[pin-4];
            }
            break;
        default:
            gpio = pin;
            break;
        }
        return gpio;
    }
    
    void report_version()
    {
        printf(app_version);
        printf("\n");
        printf(app_copyright);
        printf("\n");
    }

    void write_spi_bytes()
    {
        std::uint8_t  cmdVal = 0;
        std::uint8_t  valCnt = 0;
        std::uint8_t  cnt = 0;
        std::uint8_t  cmdCh = 0;
        while (! cmd_buffer.empty())
        {
            cmdCh = cmd_buffer.out();
            if (cmdCh == CMD_TERMINATOR) {
                cmdval_buffer[valCnt] = cmdVal;
                valCnt++;
                break;
            }
            else if (cmdCh == CMD_VAL_SEPARATOR) {
                cmdval_buffer[valCnt] = cmdVal;
                valCnt++;
                cmdVal = 0;
                cnt = 0;
                continue;
            }
            std::uint8_t  x = (cmdCh >= 'A') ?
                (cmdCh - 'A' + 10) : (cmdCh - '0');
            if (cnt > 0) {
                cmdVal = cmdVal << 4;  // * 16
            }
            cnt++;
            cmdVal += x;
        }

        spi_write_blocking(spi0, cmdval_buffer.data(), valCnt);
    }

    void masked_port_output()
    {
        printf("masked_port_output\n");

        std::uint8_t  port = cmd_buffer.out();
        std::uint8_t  cmdCh = 0;
        if (cmd_buffer.out() == CMD_VAL_SEPARATOR) {

            std::uint8_t  shift = 0;
            std::uint32_t  bit_mask = 0x1FFFFFFF;
            
            if (port == 'B') {
                shift = PORTB_START - 1;
                bit_mask = PORTB_MASK;
            }
            else if (port == 'D') {
                shift = PORTD_START - 1;
                bit_mask = PORTD_MASK;
            }
            else if (port == 'C') {
                shift = PORTC_START - 1;
                bit_mask = PORTC_MASK;
            }
            
            std::uint32_t  byteVal = 0;
            std::uint32_t  maskVal = 0;
            std::uint32_t  gpio_mask = 0;
            std::uint32_t  gpio_value = 0;
            std::uint8_t   cnt = 0;
            while (! cmd_buffer.empty())
            {
                cmdCh = cmd_buffer.out();
                if (cmdCh == CMD_TERMINATOR ||
                    cmdCh == CMD_VAL_SEPARATOR)
                {
                    break;
                }
                std::uint8_t  x = (cmdCh >= 'A') ?
                    (cmdCh - 'A' + 10) : (cmdCh - '0');
                if (cnt > 0) {
                    maskVal = maskVal << 4;
                }
                cnt++;
                maskVal += x;
            }
            
            gpio_mask = (maskVal << shift) & bit_mask;
            
            while (! cmd_buffer.empty())
            {
                cnt = 0;
                while (! cmd_buffer.empty())
                {
                    cmdCh = cmd_buffer.out();
                    if (cmdCh == CMD_TERMINATOR ||
                        cmdCh == CMD_VAL_SEPARATOR)
                    {
                        break;
                    }
                    std::uint8_t  x = (cmdCh >= 'A') ?
                        (cmdCh - 'A' + 10) : (cmdCh - '0');
                    if (cnt > 0) {
                        byteVal = byteVal << 4;
                    }
                    cnt++;
                    byteVal += x;
                }

                gpio_value = (byteVal << shift) & bit_mask;
                printf("  gpio_mask: %#08x, gpio_value: %#08x\n",
                       gpio_mask, gpio_value);
                printf("  gpio output: %#08x\n", gpio_mask & gpio_value);

                gpio_put_masked(gpio_mask, gpio_value);
                    
                if (cmdCh == CMD_TERMINATOR)
                {
                    break;
                }
            }
        }
    }

    void read_spi_byte()
    {
        std::uint8_t  inVal = 0;
        std::uint8_t  cmdCh = cmd_buffer.out();  // the ':'
        spi_read_blocking(spi0, (uint8_t)0, &inVal, 1);
        resp_buffer.push_front(hexchars[(inVal & 0xF0) >> 4]);
        resp_buffer.push_front(hexchars[(inVal & 0x0F)]);
    }

    void set_pin_high()
    {
        uint  gpio = pin_to_gpio();
        gpio_put(gpio, true);
    }

    void set_pin_low()
    {
        uint  gpio = pin_to_gpio();
        gpio_put(gpio, false);
    }

    void toggle_pin()
    {
        uint  gpio = pin_to_gpio();
        bool  state = gpio_get(gpio);
        gpio_put(gpio, !state);
    }

    void report_pin_status()
    {
        uint  gpio = pin_to_gpio();
        bool  state = gpio_get(gpio);
        if (state) {
            resp_buffer.push_front('1');
        }
        else {
            resp_buffer.push_front('0');
        }
    }

    void set_pin_as_output()
    {
        uint  gpio = pin_to_gpio();
        gpio_set_dir(gpio, true);
    }

    void set_pin_as_input()
    {
        uint  gpio = pin_to_gpio();
        gpio_set_dir(gpio, false);
    }

    void set_spi_clock()
    {
        current_spi_baudrate = cmd_buffer.out() - '0';
        uint8_t  cmdCh = cmd_buffer.out();  // the CMD_TERMINATOR
        spi_set_baudrate(spi0, spi_baudrates[current_spi_baudrate]);
    }

    void report_spi_clock()
    {
        uint8_t  cmdCh = cmd_buffer.out();  // the CMD_TERMINATOR
        uint rate = spi_get_baudrate(spi0);
        resp_buffer.push_front(rate + '0');
    }

    void delay_microsecs()
    {
        std::uint8_t   cmdCh = 0;
        std::uint8_t   cnt = 0;
        std::uint64_t  microsecs = 0;
        while (! cmd_buffer.empty())
        {
            cmdCh = cmd_buffer.out();
            if (cmdCh == CMD_TERMINATOR) {
                break;
            }
            std::uint8_t  x = (cmdCh >= 'A') ?
                (cmdCh - 'A' + 10) : (cmdCh - '0');
            if (cnt > 0) {
                microsecs = microsecs << 4;
            }
            cnt++;
            microsecs += x;
        }
        sleep_us(microsecs);
    }
    
    void receive_cmd()
    {
        int  ch;

        while (ch = getchar_timeout_us(cmdchar_timeout))
        {            
            if (ch == PICO_ERROR_TIMEOUT)
                continue;

            std::uint8_t  cmdCh = (std::uint8_t)ch;
            
            if (cmdCh == '\r') {
                cmd_state = cmd_available;
                break;
            }
            else {
                cmd_buffer.push_front(::toupper(cmdCh));
            }            
        }
    }

    void process_cmd()
    {
        if (cmd_buffer.empty())
            resp_msg(std::string("No cmd"));
        else
        {
            while (! cmd_buffer.empty())
            {
                uint8_t cmdCh = cmd_buffer.out();
                
                switch (cmdCh)
                {
                case 'V':
                    report_version();
                    break;
                case 'W':
                    write_spi_bytes();
                    break;
                case 'M':
                    masked_port_output();
                    break;
                case 'R':
                    read_spi_byte();
                    break;
                case 'H':
                    set_pin_high();
                    break;
                case 'L':
                    set_pin_low();
                    break;
                case 'T':
                    toggle_pin();
                    break;
                case 'P':
                    report_pin_status();
                    break;
                case 'O':
                    set_pin_as_output();
                    break;
                case 'I':
                    set_pin_as_input();
                    break;
                case 'S':
                    set_spi_clock();
                    break;
                case 'C':
                    report_spi_clock();
                    break;
                case 'D':
                    delay_microsecs();
                    break;
                default:
                    resp_msg(std::string("Unknown cmd"));
                    break;
                }
            }
        }
        resp_buffer.push_front('.');
        cmd_state = cmd_processed;        
    }

    void send_cmd_response()
    {
        for (auto it = resp_buffer.crbegin();
            it != resp_buffer.crend();
            ++it)
        {
            putchar(*it);
        }        
    }
}


int main()
{
    cmdproc_init();
    
    for (;;)
    {        
        if (cmd_state == awaiting_cmd)
        {
            receive_cmd();
        }
        else if (cmd_state == cmd_available)
        {
            process_cmd();
        }
        else if (cmd_state == cmd_processed)
        {
            send_cmd_response();
            resp_buffer.clear();
            cmd_state = awaiting_cmd;        
        }
    }
    return 0;
}

