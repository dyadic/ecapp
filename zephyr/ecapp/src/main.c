/*
 *
 */

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <zephyr/device.h>
#include <zephyr/drivers/uart.h>
#include <zephyr/drivers/gpio.h>
#include <zephyr/drivers/spi.h>
#include <zephyr/kernel.h>
#include <zephyr/sys/ring_buffer.h>

#include <zephyr/usb/usb_device.h>
#include <zephyr/usb/usbd.h>
#include <zephyr/logging/log.h>


LOG_MODULE_REGISTER(cdc_acm_echo, LOG_LEVEL_DBG);

static const uint8_t app_version[] = "ECApp USB command processor (v0.2-zephyr).";
static const uint8_t app_copyright[] = "Copyright, Dyadic Pty Ltd (2023).";

static const uint8_t hexchars[16] = {
    '0', '1', '2', '3', '4', '5', '6', '7',
    '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'
};
    
#define RECV_BUF_SIZE 1024
#define RESP_BUF_SIZE 128
#define CMDVAL_BUF_SIZE 64
#define PIN_SPEC_LEN 4
static uint8_t recv_buffer[RECV_BUF_SIZE];
static uint8_t resp_buffer[RESP_BUF_SIZE];

static struct ring_buf recvbuf;
static struct ring_buf respbuf;

static const uint8_t  CMD_TERMINATOR = ':';
static const uint8_t  CMD_VAL_SEPARATOR = ',';

const uint8_t  SOURCE_HOST = 0;
const uint8_t  SOURCE_APPTHREAD = 1;

struct command
{
    uint8_t  buf[RECV_BUF_SIZE];
    uint32_t  ch_count;
    uint32_t  source;
};

struct cmd_values
{
    uint8_t  data[CMDVAL_BUF_SIZE];
    uint8_t  count;
};
static struct cmd_values cmdval_buffer;

char __aligned(4)  cmd_msgq_buffer[4 * sizeof(struct command)];
struct k_msgq  cmd_msgq;

char __aligned(4)  app_msgq_buffer[4 * sizeof(struct command)];
struct k_msgq  app_msgq;


#define CMD_PROC_STACK_SIZE 1024
#define CMD_EXEC_STACK_SIZE 1024
#define APP_THREAD_STACK_SIZE 1024

#define CMD_PROC_PRIORITY 5
#define CMD_EXEC_PRIORITY 5
#define APP_THREAD_PRIORITY 5

K_THREAD_STACK_DEFINE(cmd_proc_stack_area, CMD_PROC_STACK_SIZE);
static struct k_thread cmd_proc_thread_data;

K_THREAD_STACK_DEFINE(cmd_exec_stack_area, CMD_EXEC_STACK_SIZE);
static struct k_thread cmd_exec_thread_data;

K_THREAD_STACK_DEFINE(app_thread_stack_area, APP_THREAD_STACK_SIZE);
static struct k_thread app_thread_thread_data;

struct k_sem  cmd_execution_sem;
K_SEM_DEFINE(cmd_execution_sem, 1, 1);

static const struct device *usb_uart = DEVICE_DT_GET_ONE(zephyr_cdc_acm_uart);


// GPIOs
//  0, 1, 5, .., 22, 26
//  0b0000_0100_0111_1111_1111_1111_1110_0000
//  0x047FFFE3
//
static const uint32_t  GPIO_MASK = 0x047FFFE0;

static const uint8_t  LED_GPIO = 25;

static const uint8_t  PORTB_START = 16;
static const uint8_t  PORTC_START = 5;
static const uint8_t  PORTD_START = 8;
static const uint32_t  PORTB_MASK = 0x047F0000;
static const uint32_t  PORTC_MASK = 0x000000E0;
static const uint32_t  PORTD_MASK = 0x0000FF00;

static const struct device *gpio0 = DEVICE_DT_GET(DT_NODELABEL(gpio0));


static struct spi_cs_control spi0_cs_ctrl;
static struct spi_config *spi0_config;
static struct spi_config spi0_config_list[5];
static const struct device *spi0 = DEVICE_DT_GET(DT_NODELABEL(spi0));
static const uint32_t spi_baudrates[] = {
    8000*1000, 4000*1000, 1000*1000, 250*1000, 100*1000
};
static const uint8_t  SPI_125K = 4;
static const uint8_t  SPI_4MEG = 1;
static const uint8_t  SPI_8MEG = 0;
static uint8_t spi0_current_baudrate;


static void resp_msg(const uint8_t *msg, uint32_t msg_len, uint8_t cmd_src)
{
    ARG_UNUSED(cmd_src);
    
    ring_buf_put(&respbuf, msg, msg_len);
    uart_irq_tx_enable(usb_uart);
}


static void initialize_spi()
{
    // Note that setting the gpio_dt_spec.port device to NULL
    // indicates that CS control is inhibited.
    spi0_cs_ctrl.gpio.port = NULL;
    spi0_cs_ctrl.delay = 0u;

    for (int i=0; i<(sizeof(spi_baudrates)/sizeof(uint32_t)); i++)
    {
        spi0_config_list[i].frequency = spi_baudrates[i];
        spi0_config_list[i].operation =
            SPI_OP_MODE_MASTER | SPI_TRANSFER_MSB | SPI_WORD_SET(8);
        spi0_config_list[i].slave = 0;
        spi0_config_list[i].cs = spi0_cs_ctrl;
    }
    spi0_current_baudrate = SPI_4MEG;
    spi0_config = &spi0_config_list[SPI_4MEG];
}


static uint8_t *set_spi_clock_rate(uint8_t *cptr)
{
    uint8_t  rate = *cptr++ - '0';

    if (rate > SPI_125K) {
        rate = SPI_125K;
    }
    if (rate < SPI_8MEG) {
        rate = SPI_8MEG;
    }
    spi0_current_baudrate = rate;
    spi0_config = &spi0_config_list[rate];
    cptr++; // The ':'
    return cptr;
}

static uint8_t *get_spi_clock_rate(uint8_t *cptr, uint8_t *rate)
{
    cptr++;  // The ':'
    *rate = spi0_current_baudrate + '0';
    return cptr;
}


static void initialize_gpio()
{
    // All relevant GPIOs are initialized as outputs and
    // set to low.
    uint8_t  pin = 0;
    int  ret = 0;

    for (pin = 0; pin < 32; pin++) {
        if (((1U << pin) & GPIO_MASK) != 0) {
            ret = gpio_pin_configure(gpio0, pin, GPIO_OUTPUT);
            if (ret) {
                LOG_ERR("Error %d: GPIO pin %d init failed", ret, pin);
            }
            ret = gpio_pin_set(gpio0, pin, 0);
            if (ret) {
                LOG_ERR("Error %d: Failed to set GPIO pin %d", ret, pin);
            }
        }
    }
}


/*! \brief Convert port and pin specification to a GPIO
 *
 * This maps an AVR port and pin specification to a GPIO on the Pico.
 * This allows client apps (which use the AVR port and pin spec.)
 * to transparently use the Pico with no changes.
 *
 * Note that valid port 'C' pins are {5, 6, 7}.  If some other
 * pin value is specified for port 'C' the result will be either
 * an exception or undefined.
 *
 */
static uint8_t *pin_to_gpio(uint8_t *cptr, uint8_t *gpio)
{
    uint8_t  cmd_ch = 0;
    uint8_t  pin = 0;
    uint8_t  port = *cptr++;
        
    if (port == 'G') {
        uint8_t  cnt = 0;            
        while ((cmd_ch = *cptr++) != CMD_TERMINATOR)
        {
            uint8_t x = cmd_ch - '0';
            if (cnt > 0) {
                pin = pin * 10;
            }
            cnt++;
            pin += x;
        }
    }
    else {
        pin = *cptr++ - '0';
        cmd_ch = *cptr++;  // the CMD_TERMINATOR
    }
        
        
    switch (port)
    {
    case 'B':
        if (pin == 7) {
            *gpio = 26;
        }
        else {
            *gpio = pin + PORTB_START;
        }
        break;
    case 'D':
        *gpio = pin + PORTD_START;
        break;
    case 'C':
        *gpio = (pin - 5) + PORTC_START;
        break;
    default:
        *gpio = pin;
        break;
    }
    return cptr;
}


static uint8_t *set_pin_high(uint8_t *cptr)
{
    uint8_t  pin = 0;
    uint8_t *new_cptr = pin_to_gpio(cptr, &pin);

    int ret = gpio_pin_set(gpio0, pin, 1);
    if (ret) {
        LOG_ERR("Error %d: Failed to set GPIO pin %d high", ret, pin);
    }

    LOG_INF("set_pin_high: pin=%d", pin);
    return new_cptr;
}

static uint8_t *set_pin_low(uint8_t *cptr)
{
    uint8_t  pin = 0;
    uint8_t *new_cptr = pin_to_gpio(cptr, &pin);

    int ret = gpio_pin_set(gpio0, pin, 0);
    if (ret) {
        LOG_ERR("Error %d: Failed to set GPIO pin %d low", ret, pin);
    }

    LOG_INF("set_pin_low: pin=%d", pin);
    return new_cptr;
}

static uint8_t *toggle_pin(uint8_t *cptr)
{
    uint8_t  pin = 0;
    uint8_t *new_cptr = pin_to_gpio(cptr, &pin);

    int ret = gpio_pin_toggle(gpio0, pin);
    if (ret) {
        LOG_ERR("Error %d: Failed to toggle GPIO pin %d", ret, pin);
    }
    
    LOG_INF("toggle_pin: pin=%d", pin);
    return new_cptr;
}

static uint8_t *report_pin_status(uint8_t *cptr, int *pin_val)
{
    uint8_t  pin = 0;
    uint8_t *new_cptr = pin_to_gpio(cptr, &pin);

    *pin_val = gpio_pin_get(gpio0, pin);
    if (*pin_val < 0) {
        LOG_ERR("Error %d: Failed to get pin %d level", *pin_val, pin);
    }

    LOG_INF("report_pin_status: pin=%d", pin);
    return new_cptr;
}


static uint8_t *set_pin_as_output(uint8_t *cptr)
{
    uint8_t  pin = 0;
    uint8_t *new_cptr = pin_to_gpio(cptr, &pin);

    int ret = gpio_pin_configure(gpio0, pin, GPIO_OUTPUT);
    if (ret) {
        LOG_ERR("Error %d: Failed to set pin %d as output", ret, pin);
    }

    LOG_INF("set_pin_as_output: pin=%d", pin);
    return new_cptr;
}


static uint8_t *set_pin_as_input(uint8_t *cptr)
{
    uint8_t  pin = 0;
    uint8_t *new_cptr = pin_to_gpio(cptr, &pin);
    /* gpio_set_dir(gpio, false); */

    int ret = gpio_pin_configure(gpio0, pin, GPIO_INPUT);
    if (ret) {
        LOG_ERR("Error %d: Failed to set pin %d as input", ret, pin);
    }

    LOG_INF("set_pin_as_input: pin=%d", pin);
    return new_cptr;
}


static uint8_t *set_port_pins(uint8_t *cptr)
{
    uint8_t  port = *cptr++;
    uint8_t  cmdCh = 0;
    if (*cptr++ == CMD_VAL_SEPARATOR)
    {
        uint8_t  shift = 0;
        uint32_t  bit_mask = 0x1FFFFFFF;
            
        if (port == 'B') {
            shift = PORTB_START - 1;
            bit_mask = PORTB_MASK;
        }
        else if (port == 'D') {
            shift = PORTD_START - 1;
            bit_mask = PORTD_MASK;
        }
        else if (port == 'C') {
            shift = PORTC_START - 1;
            bit_mask = PORTC_MASK;
        }
            
        uint32_t  byteVal = 0;
        uint32_t  maskVal = 0;
        uint32_t  gpio_mask = 0;
        uint32_t  gpio_value = 0;
        uint8_t   cnt = 0;
        while (*cptr != '\0')
        {
            cmdCh = *cptr++;
            if (cmdCh == CMD_TERMINATOR ||
                cmdCh == CMD_VAL_SEPARATOR)
            {
                break;
            }
            uint8_t  x = (cmdCh >= 'A') ?
                (cmdCh - 'A' + 10) : (cmdCh - '0');
            if (cnt > 0) {
                maskVal = maskVal << 4;
            }
            cnt++;
            maskVal += x;
        }
            
        gpio_mask = (maskVal << shift) & bit_mask;
            
        while (*cptr != '\0')
        {
            cnt = 0;
            while (*cptr != '\0')
            {
                cmdCh = *cptr++;
                if (cmdCh == CMD_TERMINATOR ||
                    cmdCh == CMD_VAL_SEPARATOR)
                {
                    break;
                }
                uint8_t  x = (cmdCh >= 'A') ?
                    (cmdCh - 'A' + 10) : (cmdCh - '0');
                if (cnt > 0) {
                    byteVal = byteVal << 4;
                }
                cnt++;
                byteVal += x;
            }

            gpio_value = (byteVal << shift) & bit_mask;
            printf("  gpio_mask: %#08x, gpio_value: %#08x\n",
                   gpio_mask, gpio_value);
            printf("  gpio output: %#08x\n", gpio_mask & gpio_value);

            int ret = gpio_port_set_masked_raw(gpio0, gpio_mask, gpio_value);
            if (ret < 0) {
                LOG_ERR("Error %d: Failed to set GPIO pins", ret);
            }
                    
            if (cmdCh == CMD_TERMINATOR)
            {
                break;
            }
        }
    }
    return cptr;
}


static void interrupt_handler(const struct device *dev, void *user_data)
{
    int rx = 0;
    uint8_t *dst;
    uint32_t partial_size = 0;
    uint32_t total_size = 0;

    ARG_UNUSED(user_data);

    while (uart_irq_update(dev) && uart_irq_is_pending(dev))
    {
        if (uart_irq_rx_ready(dev)) {
            if (!partial_size) {
                partial_size = ring_buf_put_claim(&recvbuf, &dst, UINT32_MAX);
            }
            if (!partial_size) {
                LOG_ERR("USB uart recv buffer doesn't have enough space");
                break;
            }

            rx = uart_fifo_read(dev, dst, partial_size);
            if (rx <= 0) {
                continue;
            }

            dst += rx;
            total_size += rx;
            partial_size -= rx;
        }

        if (uart_irq_tx_ready(dev)) {
            uint8_t buffer[64];
            int rb_len, send_len;

            rb_len = ring_buf_get(&respbuf, buffer, sizeof(buffer));
            if (!rb_len) {
                LOG_DBG("Ring buffer empty, disable TX IRQ");
                uart_irq_tx_disable(dev);
                continue;
            }

            send_len = uart_fifo_fill(dev, buffer, rb_len);
            if (send_len < rb_len) {
                LOG_ERR("Drop %d bytes", rb_len - send_len);
            }

            LOG_DBG("ringbuf -> tty fifo %d bytes", send_len);
        }
        
    }
    ring_buf_put_finish(&recvbuf, total_size);    
}


static void write_spi_bytes(struct cmd_values *valbuf_ptr)
{
    struct spi_buf tx_buf = { .buf = valbuf_ptr->data, .len = valbuf_ptr->count };
    struct spi_buf_set tx_bufs = { .buffers = &tx_buf, .count = 1 };

    LOG_DBG("write_spi_bytes: freq=%d", spi0_config->frequency);
    int  ret = spi_write(spi0, spi0_config, &tx_bufs);
    if (ret < 0) {
        LOG_ERR("Error %d: Failed to write SPI bytes", ret);
    }
}


static uint8_t read_spi_byte()
{
    struct spi_buf tx_buf = { .buf = NULL, .len = 1 };
    struct spi_buf_set tx_bufs = { .buffers = &tx_buf, .count = 1 };

    uint8_t rxdata[1];
    struct spi_buf rx_buf = { .buf = rxdata, .len = 1 };
    struct spi_buf_set rx_bufs = { .buffers = &rx_buf, .count = 1 };

    int ret = spi_transceive(spi0, spi0_config, &tx_bufs, &rx_bufs);
    if (ret < 0) {
        LOG_ERR("Error %d: Failed to read SPI byte", ret);
    }
    return rxdata[0];
}

static void app_response(uint8_t *cptr)
{
    static struct command resp_buffer;
    struct command *resp_buf = &resp_buffer;
            
    resp_buf->source = SOURCE_APPTHREAD;
    resp_buf->buf[0] = 'A';
    resp_buf->ch_count = 1;
    while (*cptr != '\0')
    {
        resp_buf->buf[resp_buf->ch_count++] = *cptr++;
    }
    resp_buf->buf[resp_buf->ch_count++] = ':';
    resp_buf->buf[resp_buf->ch_count] = '\0';

    if (resp_buf->ch_count > 0) {
        while (k_msgq_put(&cmd_msgq, resp_buf, K_NO_WAIT) != 0) {
            /* message queue is full: purge old data & try again */
            k_msgq_purge(&cmd_msgq);
        }
    }
}

#pragma GCC push_options
#pragma GCC optimize ("O0")

static uint16_t read_pwr_detector(uint8_t *cs_pin)
{
    uint8_t  spi_rate[] = "3";
    struct cmd_values  spi_cmd = { .data[0] = 0x40, .count = 1 };

    // SPI clock speed for the attiny45 should be <500kHz
    (void)set_spi_clock_rate(spi_rate);

    // Enable attiny SPI
    (void)set_pin_low(cs_pin);

    k_sleep(K_TICKS(1));
    
    // Start an ADC conversion
    write_spi_bytes(&spi_cmd);

    // Pause for 0.25 millisecs.
    k_sleep(K_TICKS(10));

    // Read ADC low byte
    spi_cmd.data[0] = 0x01;
    write_spi_bytes(&spi_cmd);
    k_sleep(K_TICKS(1));
    uint8_t  low_byte = read_spi_byte();

    k_sleep(K_TICKS(1));

    // Read ADC high byte
    spi_cmd.data[0] = 0x02;
    write_spi_bytes(&spi_cmd);
    k_sleep(K_TICKS(1));
    uint8_t  high_byte = read_spi_byte();

    // Disable attiny SPI
    (void)set_pin_high(cs_pin);
    
    // SPI clock speed is returned to default
    spi_rate[0] = '1';
    (void)set_spi_clock_rate(spi_rate);

    uint16_t  level = ((uint16_t)high_byte) << 8;
    level += low_byte;
    return level;
}

static uint16_t adjust_level(uint8_t *cs_pin)
{
    uint16_t  level = 0;
    
    int  ret = k_sem_take(&cmd_execution_sem, K_FOREVER);
    if (ret < 0) {
        LOG_ERR("Error %d: Failed to take command semaphore", ret);
    }
        
    for (uint8_t i=0; i<8; i++) {
        level += read_pwr_detector(cs_pin);
    }
    level >>= 3; 

    k_sem_give(&cmd_execution_sem);

    return level;
}

static void app_thread(void *p1, void *p2, void *p3)
{
    ARG_UNUSED(p1);
    ARG_UNUSED(p2);
    ARG_UNUSED(p3);
    static uint8_t  det_cs_pin[PIN_SPEC_LEN] = "D0";
    static uint8_t  attn_le_pin[PIN_SPEC_LEN] = "D1";
    static uint8_t  running = 0;
    static uint16_t  reqd_level;
    static struct command  cmd_buffer;
    struct command  *cmd_buf = &cmd_buffer;
    
    while (true)
    {
        int ret = k_msgq_peek(&app_msgq, cmd_buf);
        if (ret >= 0) {
            // Process application message
            ret = k_msgq_get(&app_msgq, cmd_buf, K_FOREVER);
            if (ret < 0) {
                LOG_ERR("Error %d: Failed to read app command message", ret);
                continue;
            }

            uint8_t  *cptr = cmd_buf->buf;
            while (*cptr != '\0')
            {
                uint8_t  cmd_ch = *cptr++;

                if (cmd_ch == 'B')
                {
                    running = 1;
                }
                else if (cmd_ch == 'E')
                {
                    running = 0;
                }
                else if (cmd_ch == 'L')
                {
                    // Set the required output signal level
                    reqd_level = atoi(cptr);
                    while (*cptr++ != '\0')
                        ;
                }
                else if (cmd_ch == 'P')
                {
                    // Set the pin mappings for the power detector and
                    // step attenuator
                    uint8_t  count = 0;
                    while (*cptr != '\0' && count < PIN_SPEC_LEN-1)
                    {
                        cmd_ch = *cptr++;
                        if (cmd_ch == CMD_VAL_SEPARATOR) {
                            break;
                        }
                        det_cs_pin[count++] = cmd_ch;
                    }
                    det_cs_pin[count] = '\0';

                    count = 0;
                    while (*cptr != '\0' && count < PIN_SPEC_LEN-1)
                    {
                        cmd_ch = *cptr++;
                        attn_le_pin[count++] = cmd_ch;
                    }                    
                    attn_le_pin[count] = '\0';
                }
                else if (cmd_ch == 'M')
                {
                    uint16_t  level = 0;

                    for (uint8_t i=0; i<8; i++) {
                        level += read_pwr_detector(det_cs_pin);
                    }
                    level >>= 3;
                }
            }
        }

        // Application code
        if (running > 0) {
            uint16_t actual_level = adjust_level(det_cs_pin);
        }
        
        k_sleep(K_MSEC(1));
    }
}

#pragma GCC pop_options

static void execute_cmd(void *p1, void *p2, void *p3)
{
    ARG_UNUSED(p1);
    ARG_UNUSED(p2);
    ARG_UNUSED(p3);
    static struct command cmd_buffer;
    struct command  *cmd_buf = &cmd_buffer;
    
    while (true)
    {
        /* get a data item */
        int ret = k_msgq_get(&cmd_msgq, cmd_buf, K_FOREVER);
        if (ret < 0) {
            LOG_ERR("Error %d: Failed to read command message", ret);
            continue;
        }

        ret = k_sem_take(&cmd_execution_sem, K_FOREVER);
        if (ret < 0) {
            LOG_ERR("Error %d: Failed to take command semaphore", ret);
        }
        
        uint8_t  *cptr = cmd_buf->buf;
        uint32_t   cmd_src = cmd_buf->source;
        while (*cptr != '\0')
        {
            uint8_t  cmd_ch = *cptr++;

            if (cmd_ch == 'V')
            {
                uint8_t  newline[] = "\n";
                resp_msg(app_version, strlen(app_version), cmd_src);
                resp_msg(newline, 1, cmd_src);
                resp_msg(app_copyright, strlen(app_copyright), cmd_src);
                resp_msg(newline, 1, cmd_src);
            }
            else if (cmd_ch == 'W')
            {
                struct cmd_values  *valbuf_ptr = &cmdval_buffer;
                uint8_t  cmd_val = 0;
                uint8_t  cnt = 0;

                valbuf_ptr->count = 0;
            
                while (*cptr != '\0')
                {
                    cmd_ch = *cptr++;
                    if (cmd_ch == CMD_TERMINATOR) {
                        valbuf_ptr->data[valbuf_ptr->count++] = cmd_val;
                        break;
                    }
                    else if (cmd_ch == CMD_VAL_SEPARATOR) {
                        valbuf_ptr->data[valbuf_ptr->count++] = cmd_val;
                        cmd_val = 0;
                        cnt = 0;
                        continue;
                    }
                    uint8_t  x = (cmd_ch >= 'A') ? (cmd_ch - 'A' + 10) : (cmd_ch - '0');
                    if (cnt > 0) {
                        cmd_val = cmd_val << 4;  // * 16
                    }
                    cnt++;
                    cmd_val += x;
                }
                write_spi_bytes(valbuf_ptr);
            }
            else if (cmd_ch == 'M')
            {
                cptr = set_port_pins(cptr);
            }
            else if (cmd_ch == 'R')
            {
                ++cptr;  // ':'
                uint8_t  inVal = read_spi_byte();
                uint8_t  inChr;
                inChr = hexchars[(inVal & 0xF0) >> 4];
                resp_msg(&inChr, 1, cmd_src);
                inChr = hexchars[(inVal & 0x0F)];
                resp_msg(&inChr, 1, cmd_src);
            }
            else if (cmd_ch == 'H')
            {
                cptr = set_pin_high(cptr);
            }
            else if (cmd_ch == 'L')
            {
                cptr = set_pin_low(cptr);
            }
            else if (cmd_ch == 'T')
            {
                cptr = toggle_pin(cptr);
            }
            else if (cmd_ch == 'P')
            {
                int  pin_val;
                cptr = report_pin_status(cptr, &pin_val);
                if (pin_val) {
                    resp_msg("1", 1, cmd_src);
                }
                else {
                    resp_msg("0", 1, cmd_src);
                }
            }
            else if (cmd_ch == 'O')
            {
                cptr = set_pin_as_output(cptr);
            }
            else if (cmd_ch == 'I')
            {
                cptr = set_pin_as_input(cptr);
            }
            else if (cmd_ch == 'S')
            {
                cptr = set_spi_clock_rate(cptr);
            }
            else if (cmd_ch == 'C')
            {
                uint8_t  rate;
                cptr = get_spi_clock_rate(cptr, &rate);
                resp_msg(&rate, 1, cmd_src);
            }
            else if (cmd_ch == 'D')
            {
                LOG_INF("Delay by specified microsecs");
            }
            else if (cmd_ch == 'A')
            {
                static struct command appcmd_buffer;
                struct command *appcmd_buf = &appcmd_buffer;

                if (cmd_src == SOURCE_HOST)
                {
                    appcmd_buf->ch_count = 0;
                    while (*cptr != CMD_TERMINATOR) {
                        appcmd_buf->buf[appcmd_buf->ch_count++] = *cptr++;
                    }
                    ++cptr;  // The ':'
                    appcmd_buf->buf[appcmd_buf->ch_count++] = '\0';

                    if (appcmd_buf->ch_count > 0) {
                        int ret = k_msgq_put(&app_msgq, appcmd_buf, K_NO_WAIT);
                        if (ret < 0) {
                            LOG_ERR("Error %d: Failed to queue app. cmd", ret);
                        }
                    }
                }
                else
                {
                    uint8_t   resp_buffer[RESP_BUF_SIZE];
                    uint8_t  *resp_buf = resp_buffer;
                    
                    /* Source is SOURCE_APPTHREAD */
                    while (*cptr != CMD_TERMINATOR) {
                        *resp_buf++ = *cptr++;
                    }
                    ++cptr;  // The ':'
                    *resp_buf = '\0';
                    resp_msg(resp_buffer, strlen(resp_buffer), cmd_src);
                }
            }
            else {
                LOG_INF("Unknown command: %c", cmd_ch);
            }
        }
        resp_msg(".", 1, cmd_src);

        k_sem_give(&cmd_execution_sem);
        k_yield();
    }
}


static void process_cmds(void *p1, void *p2, void *p3)
{
    ARG_UNUSED(p1);
    ARG_UNUSED(p2);
    ARG_UNUSED(p3);
    static struct command cmd_buffer;

    k_sleep(K_MSEC(1000));
    
    while (true)
    {
        struct command *cmd_buf = &cmd_buffer;
        uint8_t  ch;

        cmd_buf->source = SOURCE_HOST;
        cmd_buf->ch_count = 0;
        
        while (ring_buf_get(&recvbuf, &ch, 1) > 0)
        {
            if (ch == '\r') {
                /* cmd available */
                break;
            }
            else {
                uint8_t  cmd_ch = toupper(ch);
                
                cmd_buf->buf[cmd_buf->ch_count++] = cmd_ch;

                if (cmd_buf->ch_count == (sizeof(cmd_buf->buf) - 1)) {
                    LOG_ERR("Cmd buffer overflow");
                    break;
                }
            }
            cmd_buf->buf[cmd_buf->ch_count] = '\0';
        }

        if (cmd_buf->ch_count > 0) {
            while (k_msgq_put(&cmd_msgq, cmd_buf, K_NO_WAIT) != 0) {
                /* message queue is full: purge old data & try again */
                k_msgq_purge(&cmd_msgq);
            }
        }

        // We should be able to use k_yield() here since all threads
        // have the same priority
        //k_sleep(K_MSEC(1));
        k_yield();
    }
}

int main(void)
{
    uint32_t dtr = 0U;
    int ret;

    if (!device_is_ready(usb_uart)) {
        LOG_ERR("CDC ACM device not ready");
        return 0;
    }

    ret = usb_enable(NULL);
    if (ret != 0) {
        LOG_ERR("Failed to enable USB");
        return 0;
    }

    LOG_INF("Wait for DTR");

    while (true) {
        uart_line_ctrl_get(usb_uart, UART_LINE_CTRL_DTR, &dtr);
        if (dtr) {
            break;
        } else {
            /* Give CPU resources to low priority threads. */
            k_sleep(K_MSEC(100));
        }
    }

    LOG_INF("DTR set");

    if (!device_is_ready(gpio0)) {
        LOG_ERR("GPIO0 device not ready.");
        return 0;
    }

    if (!device_is_ready(spi0)) {
        LOG_ERR("SPI0 device not ready.");
        return 0;
    }
    
    ring_buf_init(&recvbuf, sizeof(recv_buffer), recv_buffer);
    ring_buf_init(&respbuf, sizeof(resp_buffer), resp_buffer);
        
    uart_irq_callback_set(usb_uart, interrupt_handler);

    /* Enable rx interrupts */
    uart_irq_rx_enable(usb_uart);

    initialize_gpio();
    initialize_spi();

    k_msgq_init(&cmd_msgq, cmd_msgq_buffer, sizeof(struct command), 4);
    k_msgq_init(&app_msgq, app_msgq_buffer, sizeof(struct command), 4);
    
    /* Create a cmd processing thread */
    k_thread_create(&cmd_proc_thread_data, cmd_proc_stack_area,
                    K_THREAD_STACK_SIZEOF(cmd_proc_stack_area),
                    process_cmds, NULL, NULL, NULL,
                    CMD_PROC_PRIORITY, 0, K_NO_WAIT);

    /* Create a cmd execution thread */
    k_thread_create(&cmd_exec_thread_data, cmd_exec_stack_area,
                    K_THREAD_STACK_SIZEOF(cmd_exec_stack_area),
                    execute_cmd, NULL, NULL, NULL,
                    CMD_EXEC_PRIORITY, 0, K_NO_WAIT);

    /* Create the app thread execution thread */
    k_thread_create(&app_thread_thread_data, app_thread_stack_area,
                    K_THREAD_STACK_SIZEOF(app_thread_stack_area),
                    app_thread, NULL, NULL, NULL,
                    APP_THREAD_PRIORITY, 0, K_NO_WAIT);
    
    return 0;
}
