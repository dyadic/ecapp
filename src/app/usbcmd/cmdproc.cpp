
///////////////////////////////////////////////////////////////////////////////
//  Copyright Dyadic Pty Ltd 2018

#include <stdio.h>
#include <ctype.h>
#include <os/os_cfg.h>
#include <mcal/mcal.h>
#include <string>
#include <array>
#include <iterator>
#include <util/utility/util_circular_buffer.h>
#include <util/utility/util_time.h>


namespace {

    const std::uint8_t  CMD_TERMINATOR     = ':';
    const std::uint8_t  CMD_VAL_SEPARATOR  = ',';
    const std::size_t   cmdbuf_size        = 512U;
    const std::size_t   cmdvalbuf_size     = 32U;
    const std::size_t   respbuf_size       = 16U;
    const std::array<uint8_t, 16U>    hexchars
    {
        { '0', '1', '2', '3', '4', '5', '6', '7',
          '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' }
    };
    const char app_version[] PROGMEM = "ECApp USB command processor (v0.95).";
    const char app_copyright[] PROGMEM = "Copyright, Dyadic Pty Ltd (2022).";
    const std::uint16_t bootloaderStart PROGMEM = (FLASHEND - 7UL);

    typedef util::circular_buffer<std::uint8_t, cmdbuf_size>   cmdbuf_type;
    typedef std::array<std::uint8_t, cmdvalbuf_size>           cmdvalbuf_type;
    typedef util::circular_buffer<std::uint8_t, respbuf_size>  respbuf_type;
    typedef mcal::serusb::serusb_communication                 serusb_type;

    cmdbuf_type     cmd_buffer;
    cmdvalbuf_type  cmdval_buffer;
    respbuf_type    resp_buffer;
    serusb_type     serusb_comm;

    void progmem_msg(mcal_progmem_uintptr_t pptr)
    {
        uint8_t ch;

        do {
            ch = mcal_memory_progmem_read_byte(pptr);
            serusb_comm.send(ch);
            pptr++;
        } while (ch);
    }

    void resp_msg(const std::string& msg)
    {
        std::for_each(msg.begin(), msg.end(), [] (char const& c) {
                                                  resp_buffer.push_front(c); });
    }

    std::uint8_t char_to_port(const std::uint8_t ch)
    {
        std::uint8_t  port;

        switch (ch) {
        case 'C':
            port = mcal::reg::portc;
            break;
        case 'D':
            port = mcal::reg::portd;
            break;
        default:
            port = mcal::reg::portb;
            break;
        }
        return port;
    }

    std::uint8_t char_to_pinp(const std::uint8_t ch)
    {
        std::uint8_t  pinp;

        switch (ch) {
        case 'C':
            pinp = mcal::reg::pinc;
            break;
        case 'D':
            pinp = mcal::reg::pind;
            break;
        default:
            pinp = mcal::reg::pinb;
            break;
        }
        return pinp;
    }

    std::uint8_t char_to_ddr(const std::uint8_t ch)
    {
        std::uint8_t  ddr;

        switch (ch) {
        case 'C':
            ddr = mcal::reg::ddrc;
            break;
        case 'D':
            ddr = mcal::reg::ddrd;
            break;
        default:
            ddr = mcal::reg::ddrb;
            break;
        }
        return ddr;        
    }
}

namespace app
{
    namespace cmdproc
    {
        typedef enum enum_cmdstate
        {
            awaiting_cmd,
            cmd_available,
            cmd_processed
        }
        cmdstate_type;

        cmdstate_type  cmd_state;
        std::size_t    cmd_count;

        void task_init();
        void task_func();
    }
}

void app::cmdproc::task_init()
{
    // Initialize the SPI comms
    mcal::spi::init();

    // Initialize RS232/USB comms link
    mcal::serusb::init();

    // Initialize the command buffer
    cmd_buffer.clear();
    resp_buffer.clear();
    cmd_state = awaiting_cmd;
    cmd_count = 0;
}

void app::cmdproc::task_func()
{
    if (cmd_state == awaiting_cmd)
    {
        // We read from the RS232/USB comms link and push the
        // characters into the cmd_buffer.  When the terminating
        // character is received the cmd_state is set to cmd_available.

        while (serusb_comm.recv_ready() > 0)
        {
            std::uint8_t  ch;

            serusb_comm.recv(ch);
            if (ch == '\r') {
                cmd_state = cmd_available;
            }
            else {
                cmd_buffer.push_front(::toupper(ch));
            }
        }
    }
    else if (cmd_state == cmd_available)
    {
        // Parse the command string.
        // For the moment we test SPI writes and reads.

        if (cmd_buffer.empty())
            // Error - no command specified
            resp_msg(std::string("No cmd"));
        else
        {
            while (! cmd_buffer.empty()) 
            {
                uint8_t cmdCh = cmd_buffer.out();

                if (cmdCh == 'V')
                {
                    progmem_msg(MCAL_PROGMEM_ADDRESSOF(app_version));
                    serusb_comm.endl();
                    progmem_msg(MCAL_PROGMEM_ADDRESSOF(app_copyright));
                    serusb_comm.endl();
                }
                else if (cmdCh == 'W')
                {
                    // Write SPI bytes
                    std::uint8_t  cmdVal = 0;
                    std::uint8_t  valCnt = 0;
                    std::uint8_t  cnt = 0;
                    while (! cmd_buffer.empty())
                    {
                        cmdCh = cmd_buffer.out();
                        if (cmdCh == CMD_TERMINATOR) {
                            cmdval_buffer[valCnt] = cmdVal;
                            valCnt++;
                            break;
                        }
                        else if (cmdCh == CMD_VAL_SEPARATOR) {
                            cmdval_buffer[valCnt] = cmdVal;
                            valCnt++;
                            cmdVal = 0;
                            cnt = 0;
                            continue;
                        }
                        std::uint8_t  x = (cmdCh >= 'A') ?
                            (cmdCh - 'A' + 10) : (cmdCh - '0');
                        if (cnt > 0) {
                            cmdVal = cmdVal << 4;  // * 16
                        }
                        cnt++;
                        cmdVal += x;
                    }
                    std::for_each_n(cmdval_buffer.begin(),
                                    valCnt,
                                    [] (std::uint8_t& v) {
                                        mcal::spi::the_spi.send(v); });
                    while (! mcal::spi::the_spi.idle())
                        ;
                }
                else if (cmdCh == 'M')
                {
                    std::uint8_t  port = char_to_port(cmd_buffer.out());
                    if (cmd_buffer.out() == CMD_VAL_SEPARATOR) {
                        std::uint8_t  maskVal = 0;
                        std::uint8_t  cnt = 0;
                        while (! cmd_buffer.empty())
                        {
                            cmdCh = cmd_buffer.out();
                            if (cmdCh == CMD_TERMINATOR ||
                                cmdCh == CMD_VAL_SEPARATOR)
                            {
                                break;
                            }
                            std::uint8_t  x = (cmdCh >= 'A') ?
                                (cmdCh - 'A' + 10) : (cmdCh - '0');
                            if (cnt > 0) {
                                maskVal = maskVal << 4;
                            }
                            cnt++;
                            maskVal += x;
                        }
                        while (! cmd_buffer.empty())
                        {
                            std::uint8_t  byteVal = 0;
                            cnt = 0;
                            while (! cmd_buffer.empty())
                            {
                                cmdCh = cmd_buffer.out();
                                if (cmdCh == CMD_TERMINATOR ||
                                    cmdCh == CMD_VAL_SEPARATOR)
                                {
                                    break;
                                }
                                std::uint8_t  x = (cmdCh >= 'A') ?
                                    (cmdCh - 'A' + 10) : (cmdCh - '0');
                                if (cnt > 0) {
                                    byteVal = byteVal << 4;
                                }
                                cnt++;
                                byteVal += x;
                            }
                            mcal::reg::reg_access_dynamic<std::uint8_t,
                                     std::uint8_t>::reg_msk(port, byteVal, maskVal);

                            if (cmdCh == CMD_TERMINATOR)
                            {
                                break;
                            }
                        }
                    }
                }
                else if (cmdCh == 'R')
                {
                    // Read SPI bytes
                    std::uint8_t  inVal = 0;
                    cmdCh = cmd_buffer.out();  // the ':'
                    mcal::spi::the_spi.clear_recv();
                    mcal::spi::the_spi.send(0x00U);
                    while (! mcal::spi::the_spi.recv_ready())
                        ;
                    mcal::spi::the_spi.recv(inVal);
                    resp_buffer.push_front(hexchars[(inVal & 0xF0) >> 4]);
                    resp_buffer.push_front(hexchars[(inVal & 0x0F)]);
                }
                else if (cmdCh == 'H')
                {
                    // Assert pin
                    std::uint8_t  port = char_to_port(cmd_buffer.out());
                    std::uint8_t  pin = cmd_buffer.out() - '0';
                    cmdCh = cmd_buffer.out();  // the ':'
                    mcal::reg::reg_access_dynamic<std::uint8_t,
                                                  std::uint8_t>::bit_set(port, pin);
                }
                else if (cmdCh == 'L')
                {
                    // Reset pin
                    std::uint8_t  port = char_to_port(cmd_buffer.out());
                    std::uint8_t  pin = cmd_buffer.out() - '0';
                    cmdCh = cmd_buffer.out();  // the ':'
                    mcal::reg::reg_access_dynamic<std::uint8_t,
                                                  std::uint8_t>::bit_clr(port, pin);
                }
                else if (cmdCh == 'T')
                {
                    // Toggle pin
                    std::uint8_t  port = char_to_port(cmd_buffer.out());
                    std::uint8_t  pin = cmd_buffer.out() - '0';
                    cmdCh = cmd_buffer.out();  // the ':'
                    mcal::reg::reg_access_dynamic<std::uint8_t,
                                                  std::uint8_t>::bit_not(port, pin);
                }
                else if (cmdCh == 'P')
                {
                    // Sample pin
                    std::uint8_t  pinp = char_to_pinp(cmd_buffer.out());
                    std::uint8_t  pin = cmd_buffer.out() - '0';
                    cmdCh = cmd_buffer.out();  // the ':'
                    bool pinVal = mcal::reg::reg_access_dynamic<std::uint8_t,
                                                  std::uint8_t>::bit_get(pinp, pin);
                    if (pinVal) {
                        resp_buffer.push_front('1');
                    }
                    else {
                        resp_buffer.push_front('0');
                    }
                }
                else if (cmdCh == 'O')
                {
                    // Set pin direction to output
                    std::uint8_t  ddr = char_to_ddr(cmd_buffer.out());
                    std::uint8_t  pin = cmd_buffer.out() - '0';
                    cmdCh = cmd_buffer.out();  // the ':'
                    mcal::reg::reg_access_dynamic<std::uint8_t,
                                                  std::uint8_t>::bit_set(ddr, pin);
                }
                else if (cmdCh == 'I')
                {
                    // Set pin direction to input
                    std::uint8_t  ddr = char_to_ddr(cmd_buffer.out());
                    std::uint8_t  pin = cmd_buffer.out() - '0';
                    cmdCh = cmd_buffer.out();  // the ':'
                    mcal::reg::reg_access_dynamic<std::uint8_t,
                                                  std::uint8_t>::bit_clr(ddr, pin);
                }
                else if (cmdCh == 'S')
                {
                    // Set the SPI clock rate
                    std::uint8_t  rate = cmd_buffer.out() - '0';
                    cmdCh = cmd_buffer.out();  // the ':'
                    mcal::spi::the_spi.set_clock_rate(rate);
                }
                else if (cmdCh == 'C')
                {
                    cmdCh = cmd_buffer.out();  // the ':'
                    resp_buffer.push_front(mcal::spi::the_spi.clock_rate() + '0');
                }
                else if (cmdCh == 'D')
                {
                    std::uint32_t  microsecs = 0;
                    std::uint8_t  cnt = 0;
                    while (! cmd_buffer.empty())
                    {
                        cmdCh = cmd_buffer.out();
                        if (cmdCh == CMD_TERMINATOR)
                        {
                            break;
                        }
                        std::uint8_t  x = (cmdCh >= 'A') ?
                            (cmdCh - 'A' + 10) : (cmdCh - '0');
                        if (cnt > 0) {
                            microsecs = microsecs << 4;
                        }
                        cnt++;
                        microsecs += x;
                    }

                    os::timer_type::blocking_delay(
                        os::timer_type::microseconds(microsecs));
                }
                else
                {
                    resp_msg(std::string("Unknown cmd"));
                }
            }
        }
        resp_buffer.push_front('.');
        cmd_state = cmd_processed;
    }
    else if (cmd_state == cmd_processed)
    {
        // Send the contents of the response buffer.
        for (auto it = resp_buffer.crbegin();
            it != resp_buffer.crend();
            ++it)
        {
            serusb_comm.send(*it);
        }
        //serusb_comm.endl();
        serusb_comm.flush();
        resp_buffer.clear();
        //cmd_buffer.clear();
        cmd_state = awaiting_cmd;
    }
}
