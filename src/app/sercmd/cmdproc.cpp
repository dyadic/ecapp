
///////////////////////////////////////////////////////////////////////////////
//  Copyright Dyadic Pty Ltd 2018

#include <stdio.h>
#include <ctype.h>
#include <mcal/mcal.h>
#include <string>
#include <array>
#include <iterator>
#include <util/utility/util_circular_buffer.h>

namespace {

    const std::uint8_t  CMD_TERMINATOR     = ':';
    const std::uint8_t  CMD_VAL_SEPARATOR  = ',';
    const std::size_t   cmdbuf_size        = 64U;
    const std::size_t   cmdvalbuf_size     = 16U;
    const std::size_t   respbuf_size       = 16U;
    const std::array<uint8_t, 16U>    hexchars
    {
        { '0', '1', '2', '3', '4', '5', '6', '7',
          '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' }
    };

    typedef util::circular_buffer<std::uint8_t, cmdbuf_size>   cmdbuf_type;
    typedef std::array<std::uint8_t, cmdvalbuf_size>           cmdvalbuf_type;
    typedef util::circular_buffer<std::uint8_t, respbuf_size>  respbuf_type;
    typedef mcal::ser::ser_communication                       ser_type;

    cmdbuf_type     cmd_buffer;
    cmdvalbuf_type  cmdval_buffer;
    respbuf_type    resp_buffer;
    ser_type        ser_comm;

    void resp_msg(const std::string& msg)
    {
        std::for_each(msg.begin(), msg.end(), [] (char const& c) {
                                                  resp_buffer.push_front(c); });
    }
}

namespace app
{
    namespace cmdproc
    {
        typedef enum enum_cmdstate
        {
            awaiting_cmd,
            cmd_available,
            cmd_processed
        }
        cmdstate_type;

        cmdstate_type  cmd_state;
        std::size_t    cmd_count;

        void task_init();
        void task_func();
    }
}

void app::cmdproc::task_init()
{
    // Initialize the SPI comms
    mcal::spi::init();

    // Initialize RS232/USB comms link
    mcal::ser::init();

    // Initialize the command buffer
    cmd_buffer.clear();
    resp_buffer.clear();
    cmd_state = awaiting_cmd;
    cmd_count = 0;
}

void app::cmdproc::task_func()
{
    if (cmd_state == awaiting_cmd)
    {
        // We read from the RS232 comms link and push the
        // characters into the cmd_buffer.  When the terminating
        // character is received the cmd_state is set to cmd_available.

        while (ser_comm.recv_ready() > 0)
        {
            std::uint8_t  ch;

            ser_comm.recv(ch);
            if (ch == '\r') {
                cmd_state = cmd_available;
            }
            else {
                cmd_buffer.push_front(::toupper(ch));
            }
        }
    }
    else if (cmd_state == cmd_available)
    {
        // Parse the command string.
        // For the moment we test SPI writes and reads.

        if (cmd_buffer.empty())
            // Error - no command specified
            resp_msg(std::string("No cmd"));
        else
        {
            while (! cmd_buffer.empty()) 
            {
                uint8_t cmdCh = cmd_buffer.out();

                if (cmdCh == 'W')
                {
                    std::uint8_t  cmdVal = 0;
                    std::uint8_t  valCnt = 0;
                    std::uint8_t  cnt = 0;
                    while (! cmd_buffer.empty())
                    {
                        cmdCh = cmd_buffer.out();
                        if (cmdCh == CMD_TERMINATOR) {
                            cmdval_buffer[valCnt] = cmdVal;
                            valCnt++;
                            break;
                        }
                        else if (cmdCh == CMD_VAL_SEPARATOR) {
                            cmdval_buffer[valCnt] = cmdVal;
                            valCnt++;
                            cmdVal = 0;
                            cnt = 0;
                            continue;
                        }
                        std::uint8_t  x = (cmdCh >= 'A') ?
                            (cmdCh - 'A' + 10) : (cmdCh - '0');
                        if (cnt > 0) {
                            cmdVal = cmdVal << 4;  // * 16
                        }
                        cnt++;
                        cmdVal += x;
                    }
                    mcal::spi::the_spi.assert_chip_enable();
                    std::for_each_n(cmdval_buffer.begin(),
                                    valCnt,
                                    [] (std::uint8_t& v) {
                                        mcal::spi::the_spi.send(v); });
                    while (! mcal::spi::the_spi.idle())
                        ;
                    mcal::spi::the_spi.reset_chip_enable();
                    resp_buffer.push_front('@');
                }
                else if (cmdCh == 'C')
                {
                    cmdCh = cmd_buffer.out();
                    std::uint8_t  cmdVal = (cmdCh >= 'A') ?
                            (cmdCh - 'A' + 10) : (cmdCh - '0');
                    cmdCh = cmd_buffer.out();  // the ':'
                    mcal::spi::the_spi.select_channel(cmdVal);
                    resp_buffer.push_front('*');
                }
                else if (cmdCh == 'A')
                {
                    cmdCh = cmd_buffer.out();  // the ':'
                    mcal::spi::the_spi.set_aux_high();
                    mcal::spi::the_spi.set_aux_low();
                }
                else if (cmdCh == 'E')
                {
                    cmdCh = cmd_buffer.out();  // the ':'
                    mcal::spi::the_spi.assert_chip_enable();
                    mcal::spi::the_spi.reset_chip_enable();                    
                }
                else if (cmdCh == 'R')
                {
                    std::uint8_t  inVal = 0;
                    cmdCh = cmd_buffer.out();  // the ':'
                    mcal::spi::the_spi.clear_recv();
                    mcal::spi::the_spi.assert_chip_enable();
                    mcal::spi::the_spi.send(0U);
                    while (! mcal::spi::the_spi.recv_ready())
                        ;
                    mcal::spi::the_spi.reset_chip_enable();
                    mcal::spi::the_spi.recv(inVal);
                    resp_buffer.push_front(hexchars[(inVal & 0x0F)]);
                    resp_buffer.push_front(hexchars[(inVal & 0xF0) >> 4]);
                }
                else
                {
                    resp_msg(std::string("Unknown cmd"));
                }
            }
        }
        resp_buffer.push_front('.');
        cmd_state = cmd_processed;
    }
    else if (cmd_state == cmd_processed)
    {
        // Send the contents of the response buffer.
        for (auto it = resp_buffer.crbegin();
            it != resp_buffer.crend();
            ++it)
        {
            ser_comm.send(*it);
        }
        ser_comm.flush();
        resp_buffer.clear();
        cmd_state = awaiting_cmd;
    }
}
