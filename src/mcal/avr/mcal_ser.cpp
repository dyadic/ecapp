///////////////////////////////////////////////////////////////////////////////
//  Copyright Dyadic Pty Ltd

#include <mcal_cpu.h>
#include <mcal_irq.h>
#include <mcal_port.h>
#include <mcal_ser.h>
#include <mcal_reg.h>

mcal::ser::ser_communication mcal::ser::the_ser;

// FUNCTION Main 
//   Enable Global Interrupts 

//   LOOP FOREVER 
//     {Do Stuff} 
//     Transmit_Data(Data) 
//     Sleep 
//   END LOOP 
// END FUNCTION 

// FUNCTION Transmit_Data(Data) 
//   Store Data into RingBuffer 
//   Enable TX Buffer Empty Interrupt (instead of Dean's TX Complete Interrupt)
// END FUNCTION 

// ISR TX_BUFFER_EMPTY_ISR 
//   IF RingBuffer NOT EMPTY 
//     UDR = RingBuffer.Next 
//   ELSE
//     Disable TX Buffer Empty Interrupt
//   END IF
// END ISR 

namespace
{
    void enable_rx_interrupt()
    {
        mcal::reg::reg_access_static<std::uint8_t,
                                     std::uint8_t,
                                     mcal::reg::ucsrb0,
                                     7U>::bit_set();
    }

    void disable_rx_interrupt()
    {
        mcal::reg::reg_access_static<std::uint8_t,
                                     std::uint8_t,
                                     mcal::reg::ucsrb0,
                                     7U>::bit_clr();
    }

    void enable_reg_empty_interrupt()
    {
        mcal::reg::reg_access_static<std::uint8_t,
                                     std::uint8_t,
                                     mcal::reg::ucsrb0,
                                     5U>::bit_set();
    }

    void disable_reg_empty_interrupt()
    {
        mcal::reg::reg_access_static<std::uint8_t,
                                     std::uint8_t,
                                     mcal::reg::ucsrb0,
                                     5U>::bit_clr();
    }

    bool reg_empty_interrupt_enabled()
    {
        return mcal::reg::reg_access_static<std::uint8_t,
                                            std::uint8_t,
                                            mcal::reg::ucsrb0,
                                            5U>::bit_get();
    }
}

void mcal::ser::init(void)
{
    SERIAL_BAUD_HI = UBRRH_VALUE;
    SERIAL_BAUD_LO = UBRRL_VALUE;

#if USE_2X
    SERIAL_STATUS |= _BV(BIT_DOUBLE_SPEED);
#else
    SERIAL_STATUS &= ~(_BV(BIT_DOUBLE_SPEED));
#endif

    // Send and receive 8-bit data.
    SERIAL_CONTROL_C = _BV(BIT_DATA_SIZE_1) | _BV(BIT_DATA_SIZE_0);

    // Enable RX and TX pins.
    SERIAL_CONTROL_B = _BV(BIT_ENABLE_RECV) | _BV(BIT_ENABLE_TRANS);

    UCSR0B |= (1 << RXCIE0); // Enable the USART Recieve Complete interrupt (USART_RXC)
}

mcal::ser::ser_communication::ser_communication()
{
}

mcal::ser::ser_communication::~ser_communication()
{
}

bool mcal::ser::ser_communication::send(const std::uint8_t byte_to_send)
{
    mcal::irq::disable_all();

    // If the buffer and the data register is empty, just write the byte
    // to the data register and return.
    if (send_buffer.empty() && reg_empty_interrupt_enabled()) {    
        mcal::reg::reg_access_dynamic<std::uint8_t,
                               std::uint8_t>::reg_set(mcal::reg::udr0, byte_to_send);
        mcal::irq::enable_all();
        return true;
    }

    // If the send_buffer is full return false.
    if (send_buffer.size() >= send_buffer.capacity())
    {
        mcal::irq::enable_all();
        return false;        
    }

    // Otherwise, write the byte_to_send to the send_buffer and ensure
    // that the data reg. empty interrupt is enabled.
    send_buffer.in(byte_to_send);
    enable_reg_empty_interrupt();
    mcal::irq::enable_all();
    return true;
}

bool mcal::ser::ser_communication::recv(std::uint8_t& byte_to_recv)
{
    disable_rx_interrupt();

    byte_to_recv = recv_buffer.out();

    enable_rx_interrupt();

    return true;
}

mcal::ser::ser_communication::size_type mcal::ser::ser_communication::recv_ready() const
{
    disable_rx_interrupt();

    // Get the count of bytes in the receive buffer.
    const size_type count = recv_buffer.size();

    enable_rx_interrupt();

    return count;
}

bool mcal::ser::ser_communication::idle() const
{
    mcal::irq::disable_all();

    const bool is_active = reg_empty_interrupt_enabled();
    
    mcal::irq::enable_all();

    return (is_active == false);
}

void __vector_18()
{
    // The usart0 receive interrupt.

    // Check for parity error 
    if (! mcal::reg::reg_access_dynamic<std::uint8_t,
                                  std::uint8_t>::bit_get(mcal::reg::ucsra0, 2U))
    {
        const std::uint8_t byte_to_recv = 
            mcal::reg::reg_access_static<std::uint8_t,
                                         std::uint8_t,
                                         mcal::reg::udr0>::reg_get();
        mcal::ser::the_ser.recv_buffer.in(byte_to_recv);
    }
    else
    {
        // Parity error, read and discard received byte
        mcal::reg::reg_access_static<std::uint8_t,
                                     std::uint8_t,
                                     mcal::reg::udr0>::reg_get();
    }
}

void __vector_19()
{
    // The usart0 data register empty interrupt

    // If interrupts are enabled, there must be more data in the output
    // buffer. Send the next byte
    std::uint8_t  c = mcal::ser::the_ser.send_buffer.out();
    mcal::reg::reg_access_dynamic<std::uint8_t,
                                  std::uint8_t>::reg_set(mcal::reg::udr0, c);

    const bool send_buffer_is_empty = mcal::ser::the_ser.send_buffer.empty();

    if (send_buffer_is_empty) {
        // Buffer empty, so disable interrupts
        disable_reg_empty_interrupt();
    }
}
