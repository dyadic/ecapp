///////////////////////////////////////////////////////////////////////////////
//  Copyright Christopher Kormanyos 2007 - 2013.
//  Distributed under the Boost Software License,
//  Version 1.0. (See accompanying file LICENSE_1_0.txt
//  or copy at http://www.boost.org/LICENSE_1_0.txt)
//

#ifndef MCAL_SER_2011_10_20_H_
  #define MCAL_SER_2011_10_20_H_

  namespace mcal
  {
    namespace ser
    {
      void init()
    }
  }


  #include <iterator>
  #include <util/utility/util_communication.h>

  extern "C" void __vector_18() __attribute__((signal, used, externally_visible));
  extern "C" void __vector_19() __attribute__((signal, used, externally_visible));

  namespace mcal
  {
    namespace ser
    {
      typedef void config_type;

      class ser_communication : public util::communication<64U>
      {
      public:
        ser_communication();
        virtual ~ser_communication();

        virtual bool send           (const std::uint8_t byte_to_send);
        virtual bool recv           (std::uint8_t& byte_to_recv);
        virtual size_type recv_ready() const;
        virtual bool idle           () const;

      private:

        // USART0 receive interrupt
        friend void ::__vector_18();

        // USART0 transmit complete interrupt
        friend void ::__vector_19();
      };

      extern ser_communication the_ser;
    }
  }

#endif // MCAL_SER_2011_10_20_H_
