///////////////////////////////////////////////////////////////////////////////
//  Copyright Christopher Kormanyos 2012 - 2015.
//  Distributed under the Boost Software License,
//  Version 1.0. (See accompanying file LICENSE_1_0.txt
//  or copy at http://www.boost.org/LICENSE_1_0.txt)
//

#include <mcal_cpu.h>
#include <mcal_irq.h>
#include <mcal_port.h>
#include <mcal_spi.h>
#include <mcal_reg.h>

mcal::spi::spi_communication mcal::spi::the_spi;

namespace
{
  // Chip select pin types
  typedef mcal::port::port_pin<std::uint8_t,
                               std::uint8_t,
                               mcal::reg::portd,
                               2U> portd2_type;

  typedef mcal::port::port_pin<std::uint8_t,
                               std::uint8_t,
                               mcal::reg::portd,
                               3U> portd3_type;

  // Aux pin type
  typedef mcal::port::port_pin<std::uint8_t,
                               std::uint8_t,
                               mcal::reg::portd,
                               4U> portd4_type;

  void enable_rx_tx_interrupt()
  {
    // Enable the spi end-of-transmission interrupt
    // by setting the spie bit in the spcr register.
    mcal::reg::reg_access_static<std::uint8_t,
                                 std::uint8_t,
                                 mcal::reg::spcr,
                                 7U>::bit_set();
  }

  void disable_rx_tx_interrupt()
  {
    // Disable the spi end-of-transmission interrupt
    // by clearing the spie bit in the spcr register.
    mcal::reg::reg_access_static<std::uint8_t,
                                 std::uint8_t,
                                 mcal::reg::spcr,
                                 7U>::bit_clr();
  }
}

void mcal::spi::init(void)
{
    // Set the special port pins ss, mosi and sck to output.
    // Note that the SS pin (pin 2) should be set as an output.
    // If so, the pin is a general output pin which does not
    // affect the SPI system.  (If the SS pin is configured as
    // an input it must be held high to ensure master SPI operation.)
    //  SS   = PB2
    //  MOSI = PB3
    //  MISO = PB4
    //  SCLK = PB5
    constexpr std::uint8_t pdir_mask =   mcal::reg::bval2
                                       | mcal::reg::bval3
                                       | mcal::reg::bval5;

    mcal::reg::reg_access_static<std::uint8_t,
                                 std::uint8_t,
                                 mcal::reg::ddrb,
                                 pdir_mask>::reg_or();

    // Enable spi as master mode, clock idle to high, etc.
    // Set the spi clock to f_osc/16 = 1MHz.
    // See Table 23-5 (p 255) of the Atmega328pb datasheet for
    // other SCK frequency settings.
    //  f_osc/4 = 4MHz is SPR0, SPR1 and SPI2X bits clear
    //constexpr std::uint8_t spcr_mask =   mcal::reg::bval0
    //                                   | mcal::reg::bval4
    //                                   | mcal::reg::bval6;
    constexpr std::uint8_t spcr_mask =   mcal::reg::bval4
                                       | mcal::reg::bval6;

    mcal::reg::reg_access_static<std::uint8_t,
                                 std::uint8_t,
                                 mcal::reg::spcr,
                                 spcr_mask>::reg_set();

    // Set the ~chip-select ports to output high.
    portd2_type::set_direction_output();
    portd2_type::set_pin_high();

    portd3_type::set_direction_output();
    portd3_type::set_pin_high();

    // Set the aux port to output and low
    portd4_type::set_direction_output();
    portd4_type::set_pin_low();
}

mcal::spi::spi_communication::spi_communication() : send_is_active(false),
                                                    channel(0U)
{
  // Set the special port pins ss, mosi and sck to output.
  // Do this even though the ss pin will not be used.
  constexpr std::uint8_t pdir_mask =   mcal::reg::bval2
                                     | mcal::reg::bval3
                                     | mcal::reg::bval5;

  mcal::reg::reg_access_static<std::uint8_t,
                               std::uint8_t,
                               mcal::reg::ddrb,
                               pdir_mask>::reg_or();

  // Enable spi as master mode, clock idle to high, etc.
  // Set the spi clock to f_osc/64 = (1/4)MHz.
  constexpr std::uint8_t spcr_mask =   mcal::reg::bval1
                                     | mcal::reg::bval2
                                     | mcal::reg::bval3
                                     | mcal::reg::bval4
                                     | mcal::reg::bval6;

  mcal::reg::reg_access_static<std::uint8_t,
                               std::uint8_t,
                               mcal::reg::spcr,
                               spcr_mask>::reg_set();

  // Set the chip-select-not ports to output high.
  portd2_type::set_pin_high();
  portd2_type::set_direction_output();

  portd3_type::set_pin_high();
  portd3_type::set_direction_output();
}

mcal::spi::spi_communication::~spi_communication()
{
}

void mcal::spi::spi_communication::assert_chip_enable()
{
    // Set this ~chip-select for the appropriate channel to low.
    if(channel == static_cast<std::uint8_t>(1U)) {
        portd3_type::set_pin_low();
    }
    else {
        portd2_type::set_pin_low();
    }
}

void mcal::spi::spi_communication::reset_chip_enable()
{
    // Reset the chip-select-not on the channel to high.
    if(channel == static_cast<std::uint8_t>(1U)) {
        portd3_type::set_pin_high();
    }
    else {
        portd2_type::set_pin_high();
    }
}

void mcal::spi::spi_communication::set_aux_high()
{
    portd4_type::set_pin_high();
}

void mcal::spi::spi_communication::set_aux_low()
{
    portd4_type::set_pin_low();
}

bool mcal::spi::spi_communication::send(const std::uint8_t byte_to_send)
{
  mcal::irq::disable_all();

  // If the spi is idle, begin transmission.
  if(send_is_active == false)
  {
    // Set the send-active flag.
    send_is_active = true;

    // Enable the spi rx/tx interrupt.
    enable_rx_tx_interrupt();

    // Send the first byte over spi.
    mcal::reg::reg_access_dynamic<std::uint8_t,
                              std::uint8_t>::reg_set(mcal::reg::spdr, byte_to_send);

    mcal::irq::enable_all();
  }
  else
  {
    // A transmission is already in progress.
    // Pack the next byte-to-send into the send-buffer.
    send_buffer.in(byte_to_send);

    mcal::irq::enable_all();
  }

  return true;
}

bool mcal::spi::spi_communication::recv(std::uint8_t& byte_to_recv)
{
  disable_rx_tx_interrupt();

  byte_to_recv = recv_buffer.out();

  enable_rx_tx_interrupt();

  return true;
}

mcal::spi::spi_communication::size_type mcal::spi::spi_communication::recv_ready() const
{
  disable_rx_tx_interrupt();

  // Get the count of bytes in the receive buffer.
  const size_type count = recv_buffer.size();

  enable_rx_tx_interrupt();

  return count;
}

bool mcal::spi::spi_communication::idle() const
{
  disable_rx_tx_interrupt();

  const bool is_active = send_is_active;

  enable_rx_tx_interrupt();

  return (is_active == false);
}

bool mcal::spi::spi_communication::select_channel(const std::uint8_t ch)
{
  // Support for two channels 0 and 1.
  if(ch <= static_cast<std::uint8_t>(2U))
  {
    channel = ch;

    return true;
  }
  else
  {
    return false;
  }
}

void __vector_17()
{
  // The spi interrupt is on end-of-transmission.

  // Receive the byte from the previous transmission.
  const std::uint8_t byte_to_recv = mcal::reg::reg_access_static<std::uint8_t,
                                                      std::uint8_t,
                                                      mcal::reg::spdr>::reg_get();

  mcal::spi::the_spi.recv_buffer.in(byte_to_recv);

  const bool send_buffer_is_empty = mcal::spi::the_spi.send_buffer.empty();

  if(send_buffer_is_empty)
  {
    // The send-buffer is empty and reception from
    // the previous (final) transmission is done.
    // Deactivate the send-active flag.
    mcal::spi::the_spi.send_is_active = false;

    // Disable the spi rx/tx interrupt.
    disable_rx_tx_interrupt();
  }
  else
  {
    // Send the next byte if there is at least one in the send queue.
    const std::uint8_t byte_to_send = mcal::spi::the_spi.send_buffer.out();

    mcal::reg::reg_access_dynamic<std::uint8_t,
                              std::uint8_t>::reg_set(mcal::reg::spdr, byte_to_send);
  }
}

