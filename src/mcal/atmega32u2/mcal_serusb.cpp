///////////////////////////////////////////////////////////////////////////////
//  Copyright Dyadic Pty Ltd 2018

#include <usb/Descriptors.h>

#include <avr/power.h>

#include <LUFA/Drivers/USB/USB.h>
#include <LUFA/Platform/Platform.h>

#include <mcal_led.h>
#include <mcal_serusb.h>


//  LUFA CDC Class driver interface configuration and state information. 
//  This structure is passed to all CDC Class driver functions, so that 
//  multiple instances of the same class within a device can be differentiated 
//  from one another.
//
//  We define an IN and OUT endpoint in addition to a notification
//  endpoint.
USB_ClassInfo_CDC_Device_t SerUSB_CDC_Interface =
{
    .Config =
    {
	.ControlInterfaceNumber   = INTERFACE_ID_CDC_CCI,
	.DataINEndpoint           =
	{
	    .Address          = CDC_TX_EPADDR,
	    .Size             = CDC_TXRX_EPSIZE,
	    .Banks            = 1,
	},
	.DataOUTEndpoint =
	{
	    .Address          = CDC_RX_EPADDR,
	    .Size             = CDC_TXRX_EPSIZE,
	    .Banks            = 1,
	},
	.NotificationEndpoint =
	{
	    .Address          = CDC_NOTIFICATION_EPADDR,
	    .Size             = CDC_NOTIFICATION_EPSIZE,
	    .Banks            = 1,
	},
    },
};

// Standard file stream for the CDC interface when set up, so that the
//  virtual CDC COM port can be used like any regular character stream in the C APIs.
//static FILE USBSerialStream;


// Event handler for the library USB Connection event.
void EVENT_USB_Device_Connect(void)
{
    mcal::led::led1.toggle();
}

// Event handler for the library USB Disconnection event.
void EVENT_USB_Device_Disconnect(void)
{
    mcal::led::led1.toggle();
    mcal::led::led2.toggle();
}

// Event handler for the library USB Configuration Changed event.
void EVENT_USB_Device_ConfigurationChanged(void)
{
    bool configSuccess = true;

    configSuccess &= CDC_Device_ConfigureEndpoints(&SerUSB_CDC_Interface);

    if (configSuccess == true)
        mcal::led::led2.toggle();
    else
        mcal::led::led1.toggle();
}

// Event handler for the library USB Control Request reception event.
void EVENT_USB_Device_ControlRequest(void)
{
    CDC_Device_ProcessControlRequest(&SerUSB_CDC_Interface);
}


void mcal::serusb::init(void)
{
    // Disable clock division
    clock_prescale_set(clock_div_1);

    // Initialize USB library
    USB_Init();

    // Create a regular character stream for the interface so that it can be
    // used with the stdio.h functions
    //CDC_Device_CreateStream(&SerUSB_CDC_Interface, &USBSerialStream);
    GlobalInterruptEnable();
}

mcal::serusb::serusb_communication::serusb_communication()
{
}

mcal::serusb::serusb_communication::~serusb_communication()
{
}

bool mcal::serusb::serusb_communication::send(const std::uint8_t byte_to_send)
{
    if (CDC_Device_SendByte(&SerUSB_CDC_Interface, byte_to_send)
          == ENDPOINT_READYWAIT_NoError)
    {
        return true;
    }
    else
    {
        return false;
    }
}

void mcal::serusb::serusb_communication::flush()
{
    CDC_Device_Flush(&SerUSB_CDC_Interface);
}

bool mcal::serusb::serusb_communication::recv(std::uint8_t& byte_to_recv)
{
    int16_t receivedCh = CDC_Device_ReceiveByte(&SerUSB_CDC_Interface);
    if (receivedCh < 0)
    {
        return false;
    }
    else
    {
        byte_to_recv = static_cast<std::uint8_t>(receivedCh);
        return true;
    }
}

std::size_t mcal::serusb::serusb_communication::recv_ready() const
{
    return CDC_Device_BytesReceived(&SerUSB_CDC_Interface);
}


