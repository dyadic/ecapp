///////////////////////////////////////////////////////////////////////////////
//  Copyright Christopher Kormanyos 2012 - 2015.
//  Distributed under the Boost Software License,
//  Version 1.0. (See accompanying file LICENSE_1_0.txt
//  or copy at http://www.boost.org/LICENSE_1_0.txt)
//

#include <mcal_cpu.h>
#include <mcal_irq.h>
#include <mcal_port.h>
#include <mcal_spi.h>
#include <mcal_reg.h>

mcal::spi::spi_communication mcal::spi::the_spi;


namespace
{
    // Enable spi as master mode, clock idle to high, etc.
    // Set the spi clock to f_osc/4 = 4MHz.
    // See Table 17-5 (p 146) of the Atmega32u2 datasheet for
    // other SCK frequency settings.
    //  f_osc/4 = 4MHz is SPR0, SPR1 and SPI2X bits clear
    //  f_osc/16 = 1MHz is SPR1 and SPI2X bits clear
    //  f_osc/64 = 0.25MHz is SPR0 and SPI2X bits clear
    //  f_osc/128 = 0.125MHz is SPI2X clear
    //
    // Note that setting SPI2X will double the SPI clock rate
    // as set by SPR0 and SPR1.

    // SPI enabled, SPI master, 0.125MHz SPI clk:
    constexpr std::uint8_t spcr_mask4 =   mcal::reg::bval0
                                        | mcal::reg::bval1
                                        | mcal::reg::bval4
                                        | mcal::reg::bval6;

    // SPI enabled, SPI master, 0.25MHz SPI clk:
    constexpr std::uint8_t spcr_mask3 =   mcal::reg::bval1
                                        | mcal::reg::bval4
                                        | mcal::reg::bval6;

    // SPI enabled, SPI master, 1MHz SPI clk:
    constexpr std::uint8_t spcr_mask2 =   mcal::reg::bval0
                                        | mcal::reg::bval4
                                        | mcal::reg::bval6;

    // SPI enabled, SPI master, 4MHz SPI clk:
    constexpr std::uint8_t spcr_mask1 =   mcal::reg::bval4
                                        | mcal::reg::bval6;

    void enable_rx_tx_interrupt()
    {
        // Enable the spi end-of-transmission interrupt
        // by setting the spie bit in the spcr register.
        mcal::reg::reg_access_static<std::uint8_t,
                                     std::uint8_t,
                                     mcal::reg::spcr,
                                     7U>::bit_set();
    }

    void disable_rx_tx_interrupt()
    {
        // Disable the spi end-of-transmission interrupt
        // by clearing the spie bit in the spcr register.
        mcal::reg::reg_access_static<std::uint8_t,
                                     std::uint8_t,
                                     mcal::reg::spcr,
                                     7U>::bit_clr();
    }
}

void mcal::spi::init(void)
{
    // Set the special port pins ss, mosi and sck to output.
    // Note that the SS pin (pin 0) should be set as an output.
    // If so, the pin is a general output pin which does not
    // affect the SPI system.  (If the SS pin is configured as
    // an input it must be held high to ensure master SPI operation.)
    //  SS   = PB0
    //  SCLK = PB1
    //  MOSI = PB2
    //  MISO = PB3
    constexpr std::uint8_t pdir_mask =   mcal::reg::bval0
                                       | mcal::reg::bval1
                                       | mcal::reg::bval2;

    mcal::reg::reg_access_static<std::uint8_t,
                                 std::uint8_t,
                                 mcal::reg::ddrb,
                                 pdir_mask>::reg_or();
    mcal::reg::reg_access_static<std::uint8_t,
                                 std::uint8_t,
                                 mcal::reg::ddrb,
                                 3U>::bit_clr();


    mcal::reg::reg_access_dynamic<std::uint8_t,
                                  std::uint8_t>::reg_set(
                                      mcal::reg::spcr, spcr_mask1);
}

mcal::spi::spi_communication::spi_communication() : send_is_active(false),
                                                    channel(0U),
                                                    clk_rate(1U)
{
}

mcal::spi::spi_communication::~spi_communication()
{
}

bool mcal::spi::spi_communication::send(const std::uint8_t byte_to_send)
{
    mcal::irq::disable_all();
    
    // If the spi is idle, begin transmission.
    if(send_is_active == false)
    {
        // Set the send-active flag.
        send_is_active = true;

        // Enable the spi rx/tx interrupt.
        enable_rx_tx_interrupt();

        // Send the first byte over spi.
        mcal::reg::reg_access_dynamic<std::uint8_t,
                                  std::uint8_t>::reg_set(mcal::reg::spdr, byte_to_send);

        mcal::irq::enable_all();
    }
    else
    {
        // A transmission is already in progress.
        // Pack the next byte-to-send into the send-buffer.
        send_buffer.in(byte_to_send);

        mcal::irq::enable_all();
    }

    return true;
}

bool mcal::spi::spi_communication::recv(std::uint8_t& byte_to_recv)
{
    disable_rx_tx_interrupt();

    byte_to_recv = recv_buffer.out();

    enable_rx_tx_interrupt();

    return true;
}

mcal::spi::spi_communication::size_type mcal::spi::spi_communication::recv_ready() const
{
    disable_rx_tx_interrupt();

    // Get the count of bytes in the receive buffer.
    const size_type count = recv_buffer.size();

    enable_rx_tx_interrupt();

    return count;
}

bool mcal::spi::spi_communication::idle() const
{
    disable_rx_tx_interrupt();

    const bool is_active = send_is_active;
    
    enable_rx_tx_interrupt();

    return (is_active == false);
}

void mcal::spi::spi_communication::set_clock_rate(const std::uint8_t rate)
{
    clk_rate = rate;

    // Set the spi clock to f_osc/4 = 4MHz.
    // See Table 17-5 (p 146) of the Atmega32u2 datasheet for
    // other SCK frequency settings.
    //  f_osc/4 = 4MHz is SPR0, SPR1 and SPI2X bits clear
    //  f_osc/16 = 1MHz is SPR1 and SPI2X bits clear
    //  f_osc/64 = 0.25MHz is SPR0 and SPI2X bits clear
    //  f_osc/128 = 0.125MHz is SPI2X clear
    //
    // Note that setting SPI2X will double the SPI clock rate
    // as set by SPR0 and SPR1.

    std::uint8_t spcr_mask;

    // Clear the double SPI clock speed bit
    mcal::reg::reg_access_dynamic<std::uint8_t,
                                  std::uint8_t>::bit_clr(
                                      mcal::reg::spsr, mcal::reg::bval0);

    if (clk_rate == 0)
    {
        // f_osc/2
        mcal::reg::reg_access_dynamic<std::uint8_t,
                                      std::uint8_t>::reg_set(
                                          mcal::reg::spcr, spcr_mask1);
        // Set the double SPI clock speed bit
        mcal::reg::reg_access_dynamic<std::uint8_t,
                                      std::uint8_t>::bit_set(
                                          mcal::reg::spsr, mcal::reg::bval0);
    }
    if (clk_rate == 1)
    {
        // f_osc/4
        mcal::reg::reg_access_dynamic<std::uint8_t,
                                      std::uint8_t>::reg_set(
                                          mcal::reg::spcr, spcr_mask1);
    }
    else if (clk_rate == 2)
    {
        // f_osc/16
        mcal::reg::reg_access_dynamic<std::uint8_t,
                                      std::uint8_t>::reg_set(
                                          mcal::reg::spcr, spcr_mask2);
    }
    else if (clk_rate == 3)
    {
        // f_osc/64
        mcal::reg::reg_access_dynamic<std::uint8_t,
                                      std::uint8_t>::reg_set(
                                          mcal::reg::spcr, spcr_mask3);
    }
    else
    {
        // f_osc/128
        mcal::reg::reg_access_dynamic<std::uint8_t,
                                      std::uint8_t>::reg_set(
                                          mcal::reg::spcr, spcr_mask4);        
    }
}

void __vector_22()
{
    // The spi interrupt is on end-of-transmission.

    // Receive the byte from the previous transmission.
    const std::uint8_t byte_to_recv = 
        mcal::reg::reg_access_dynamic<std::uint8_t, std::uint8_t>::reg_get(
            mcal::reg::spdr);

    mcal::spi::the_spi.recv_buffer.in(byte_to_recv);

    const bool send_buffer_is_empty = mcal::spi::the_spi.send_buffer.empty();

    if (send_buffer_is_empty)
    {
        // The send-buffer is empty and reception from
        // thise previous (final) transmission is done.
        // Deactivate the send-active flag.
        mcal::spi::the_spi.send_is_active = false;

        // Disable the spi rx/tx interrupt.
        disable_rx_tx_interrupt();
    }
    else
    {
        // Send the next byte if there is at least one in the send queue.
        const std::uint8_t byte_to_send = mcal::spi::the_spi.send_buffer.out();

        mcal::reg::reg_access_dynamic<std::uint8_t,
                                  std::uint8_t>::reg_set(mcal::reg::spdr, byte_to_send);
    }
}
