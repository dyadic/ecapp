///////////////////////////////////////////////////////////////////////////////
//  Copyright Christopher Kormanyos 2007 - 2013.
//  Distributed under the Boost Software License,
//  Version 1.0. (See accompanying file LICENSE_1_0.txt
//  or copy at http://www.boost.org/LICENSE_1_0.txt)
//

#ifndef MCAL_LED_2018_10_20_H_
  #define MCAL_LED_2018_10_20_H_

  #include <cstdint>
  #include <mcal_port.h>
  #include <util/utility/util_noncopyable.h>

  namespace mcal
  {
    namespace led
    {
      template<typename addr_type,
               typename reg_type,
               const addr_type port,
               const reg_type bpos>
      class led final : private util::noncopyable
      {
      public:
        led()
        {
          // Set the port pin value to low.
          port_pin_type::set_pin_low();

          // Set the port pin direction to output.
          port_pin_type::set_direction_output();
        }

        static void toggle()
        {
          // Toggle the LED.
          port_pin_type::toggle_pin();
        }

      private:
        typedef mcal::port::port_pin<addr_type,
                                     reg_type,
                                     port,
                                     bpos> port_pin_type;
      };

      typedef led<std::uint8_t,
                  std::uint8_t,
                  mcal::reg::portb,
                  UINT8_C(7)> led_b7_type;

      typedef led<std::uint8_t,
                  std::uint8_t,
                  mcal::reg::portc,
                  UINT8_C(7)> led_c7_type;

      typedef led<std::uint8_t,
                  std::uint8_t,
                  mcal::reg::portd,
                  UINT8_C(7)> led_d7_type;

      extern const led_b7_type led0;
      extern const led_c7_type led1;
      extern const led_d7_type led2;
    }
  }

#endif // MCAL_LED_2018_10_20_H_
