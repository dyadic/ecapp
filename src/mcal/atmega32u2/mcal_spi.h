///////////////////////////////////////////////////////////////////////////////
//  Copyright Christopher Kormanyos 2012 - 2015.
//  Distributed under the Boost Software License,
//  Version 1.0. (See accompanying file LICENSE_1_0.txt
//  or copy at http://www.boost.org/LICENSE_1_0.txt)
//

#ifndef MCAL_SPI_2018_11_04_H_
  #define MCAL_SPI_2018_11_04_H_

  namespace mcal
  {
    namespace spi
    {
      void init(void);
    }
  }

 
  #include <iterator>
  #include <util/utility/util_communication.h>

  extern "C" void __vector_22() __attribute__((signal, used, externally_visible));

  namespace mcal
  {
    namespace spi
    {
      typedef void config_type;

      class spi_communication : public util::communication<16U>
      {
      public:
        spi_communication();
        virtual ~spi_communication();

        virtual bool send           (const std::uint8_t byte_to_send);
        virtual bool recv           (std::uint8_t& byte_to_recv);
        virtual size_type recv_ready() const;
        virtual bool idle           () const;

        void set_clock_rate         (const std::uint8_t rate);
        std::uint8_t clock_rate     () const {
            return clk_rate;
        }

        void clear_recv() {
            recv_buffer.clear();
        }

      private:
        volatile bool send_is_active;
        std::uint8_t  channel;
        std::uint8_t  clk_rate;

        friend void ::__vector_22();
      };

      extern spi_communication the_spi;
    }
  }

#endif // MCAL_SPI_2018_11_04_H_
