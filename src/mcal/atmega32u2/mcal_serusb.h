///////////////////////////////////////////////////////////////////////////////
//  Copyright Dyadic Pty Ltd 2018

#ifndef MCAL_SERUSB_2018_10_20_H_
  #define MCAL_SERUSB_2018_10_20_H_

  #include <cstddef>
  #include <cstdint>

  namespace mcal
  {
      namespace serusb
      {
          void init(void);

          class serusb_communication
          {
          public:

              serusb_communication();
              ~serusb_communication();

              bool          send(const std::uint8_t byte_to_send);
              void          flush();
              void          endl() { send('\n'); }
              bool          recv(std::uint8_t& byte_to_recv);
              std::size_t   recv_ready() const;
          };
      }
  }

#endif // MCAL_SERUSB_2018_10_20_H_
